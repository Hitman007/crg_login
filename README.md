# README #

Wordpress has a very clunky registration system. First the user gives their email, then the system emails them a code, then the user has to copy and paste the code to login. A simpler solution is to just trust the user about their email on their first login, and just log them in. This plugin creates a sidebar widget taht asks for the user's email. If they enter a new email, they immedietly get logged in. If they enter an existing email, they are forwarded to the standard login page with their email already filled out. This pluging does away with "user names" and just uses email. It also changes the English label "Username" to "Email" in the login form. Finally, it automatically creates a "Nickname" when the user registers. It is an appended email, so "John@doe.com" would be given the automatic nickname "John".
This plugin should work on any site. Activate it and set the sidebar widget and you're ready to go.

jim@customrayguns.com

customrayguns.com
hi there